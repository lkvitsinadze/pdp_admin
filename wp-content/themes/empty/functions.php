<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

add_filter( 'acf/settings/save_json', function($path) {
    $path = get_template_directory().'/../../acf-json';
    return $path;
});

add_filter('acf/settings/load_json', function( $paths ) {
    unset($paths[0]);
    $paths[] = get_template_directory().'/../../acf-json';

    return $paths;
});

add_filter('acf/fields/google_map/api', function() {
    $api['key'] = 'AIzaSyDH17v8Kd_f-Udl2e371DRkzeuyRDjo0Pc';

    return $api;
});

function create_post_type() {
    register_taxonomy(
        'organization_type',
        'decision',
        array(
            'label' => __( 'Organization Type' ),
            'rewrite' => array( 'slug' => 'organization_type' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'dispute',
        'decision',
        array(
            'label' => __( 'Dispute' ),
            'rewrite' => array( 'slug' => 'dispute' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'result',
        'decision',
        array(
            'label' => __( 'Result' ),
            'rewrite' => array( 'slug' => 'result' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'measures_taken',
        'decision',
        array(
            'label' => __( 'Measures taken' ),
            'rewrite' => array( 'slug' => 'measures_taken' ),
            'hierarchical' => true,
        )
    );


    register_post_type( 'library',
        array(
            'labels' => array(
                'name' => __( 'Library' ),
                'singular_name' => __( 'Library' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies'  => array( 'category', 'post_tag' )
        )
    );

    register_post_type( 'press',
        array(
            'labels' => array(
                'name' => __( 'Press' ),
                'singular_name' => __( 'Press' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies'  => array( 'category', 'post_tag' )
        )
    );

    register_post_type( 'decision',
        array(
            'labels' => array(
                'name' => __( 'Decisions' ),
                'singular_name' => __( 'Decision' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies'  => array( 'organization_type', 'dispute', 'measures_taken', 'result' )
        )
    );

    register_post_type( 'recomendation',
        array(
            'labels' => array(
                'name' => __( 'Recomendations' ),
                'singular_name' => __( 'Recomendation' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

    register_post_type( 'training',
        array(
            'labels' => array(
                'name' => __( 'Trainings' ),
                'singular_name' => __( 'Training' )
            ),
            'public' => true,
            'has_archive' => false,
        )
    );

    register_post_type( 'visitor',
        array(
            'labels' => array(
                'name' => __( 'Visitors' ),
                'singular_name' => __( 'Visitor' )
            ),
            'public' => true,
            'has_archive' => false,
        )
    );

    register_post_type( 'inner_page',
        array(
            'labels' => array(
                'name' => __( 'Inner pages' ),
                'singular_name' => __( 'Inner page' )
            ),
            'public' => true,
            'has_archive' => false,
        )
    );
}

add_action( 'init', 'create_post_type' );

function add_language_code_as_meta( $post_id ) {
    // echo '<pre>';
    $lang = $_POST['icl_post_language'];
    $lang = ICL_LANGUAGE_CODE;

    update_post_meta($post_id, 'lang', $lang);
    return;
}

add_action( 'save_post', 'add_language_code_as_meta' );

/**
 * Convert values of ACF core date time pickers from Y-m-d H:i:s to timestamp
 * @param  string $value   unmodified value
 * @param  int    $post_id post ID
 * @param  object $field   field object
 * @return string          modified value
 */
function acf_save_as_timestamp( $value, $post_id, $field  ) {
    if( $value ) {
        $value = strtotime( $value );
    }

    return $value;    
}

add_filter( 'acf/update_value/type=date_picker', 'acf_save_as_timestamp', 10, 3 );

/**
 * Convert values of ACF core date time pickers from timestamp to Y-m-d H:i:s
 * @param  string $value   unmodified value
 * @param  int    $post_id post ID
 * @param  object $field   field object
 * @return string          modified value
 */
function acf_load_as_timestamp( $value, $post_id, $field  ) {
    if( $value ) {
        $value = date( 'Y-m-d H:i:s', $value );
    }

    return $value;    
}

add_filter( 'acf/load_value/type=date_picker', 'acf_load_as_timestamp', 10, 3 );

function update_term_slug($term_id, $taxonomy, $slug) {

    if(preg_match('/(-ka|-en)$/', $slug))
        return;

    $lang = ICL_LANGUAGE_CODE;

    wp_update_term($term_id, $taxonomy, array(
      'slug' => $slug.'-'.$lang
    ));
}

function taxonomy_fixing($term_id, $tt_id, $taxonomy) {
    if($taxonomy=='result' || $taxonomy=='dispute' || $taxonomy=='organization_type' || $taxonomy=='measures_taken' || $taxonomy=='post_tag'){
        $term = get_term($term_id, $taxonomy);
        update_term_slug($term_id, $taxonomy, $term->slug);
    }    
}

// do_action( "edit_{$taxonomy}", int $term_id, int $tt_id )
add_action( 'created_term', 'taxonomy_fixing', 10, 3 );
add_action( 'edited_term', 'taxonomy_fixing', 10, 3 );